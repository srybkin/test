<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Table */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DateBegin')->textInput() ?>

    <?= $form->field($model, 'DateEnd')->textInput() ?>

    <?= $form->field($model, 'IsSuccess')->checkbox() ?>

    <?= $form->field($model, 'XML')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Header')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
