<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tables');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Table'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'DateBegin:datetime',
            'DateEnd:datetime',
            'time',
            [
                'attribute'=>'IsSuccess',
                'value'=>function($data){return $data->IsSuccess ? 'Да': 'Нет';},
                'filter'=>[1=>'Да', 0=>'Нет'],
            ],
            'XML',
            // 'Header',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
