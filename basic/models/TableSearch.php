<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Table;

/**
 * TableSearch represents the model behind the search form about `app\models\Table`.
 */
class TableSearch extends Table
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['DateBegin', 'DateEnd', 'XML', 'Header'], 'safe'],
            [['IsSuccess'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Table::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'ID'=>SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'IsSuccess' => $this->IsSuccess,
        ]);

        $query->andFilterWhere(['like', 'XML', $this->XML])
            ->andFilterWhere(['like', 'Header', $this->Header])
            ->andFilterWhere(['like', 'convert(varchar,DateBegin, 104)', $this->DateBegin])
            ->andFilterWhere(['like', 'convert(varchar,DateEnd, 104)', $this->DateEnd]);

        return $dataProvider;
    }
}
