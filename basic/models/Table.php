<?php

namespace app\models;

use Yii;
use SoapClient;
use SoapParam;

/**
 * This is the model class for table "{{%table1}}".
 *
 * @property integer $ID
 * @property string $DateBegin
 * @property string $DateEnd
 * @property boolean $IsSuccess
 * @property string $XML
 * @property string $Header
 */
class Table extends \yii\db\ActiveRecord
{
    public $sample = '{"GetTaxiInfosResult":{"TaxiInfo":{"LicenseNum":"02651","LicenseDate":"08.08.2011 0:00:00","Name":"\u041e\u041e\u041e \"\u041d\u0416\u0422-\u0412\u041e\u0421\u0422\u041e\u041a\"","OgrnNum":"1107746402246","OgrnDate":"17.05.2010 0:00:00","Brand":"FORD","Model":"FOCUS","RegNum":"\u0415\u041c33377","Year":"2011","BlankNum":"002695","Info":""}}}';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%table1}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DateBegin', 'DateEnd'], 'required'],
            [['DateBegin', 'DateEnd'], 'safe'],
            [['IsSuccess'], 'boolean'],
            [['XML'], 'string', 'max' => 15000],
            [['Header'], 'string', 'max' => 3000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('ML', 'ID'),
            'DateBegin' => Yii::t('ML', 'Date Begin'),
            'DateEnd' => Yii::t('ML', 'Date End'),
            'IsSuccess' => Yii::t('ML', 'Is Success'),
            'XML' => Yii::t('ML', 'Xml'),
            'Header' => Yii::t('ML', 'Header'),
            'Time'=>Yii::t('ML', 'Time'),
        ];
    }
    
    public function save($runValidation = true, $attributeNames = null) {
        
        if($this->isNewRecord){
            $this->DateBegin = date('d.m.Y H:i:s');
            $client = new SoapClient("http://82.138.16.126:8888/TaxiPublic/Service.svc?wsdl",['trace'=> 1]);

            $s = new GetTaxiInfos;
            $s->request = new GetTaxiInfosRequest;
            $output = json_encode($client->GetTaxiInfos($s));
            $heder = json_encode($client->__getLastResponseHeaders());
            $this->DateEnd = date('d.m.Y H:i:s');
            if($output!= $this->sample){
                $this->XML = $output;
                $this->Header = $heder;
            }
            else{
                $this->IsSuccess = 1;
            }
        }
        
        return parent::save($runValidation, $attributeNames);
    }
    
    public function getTime(){
        return date('i:s', (strtotime($this->DateEnd) - strtotime($this->DateBegin)));
    }
    
    
}
class GetTaxiInfos {
    public $request;
}

class GetTaxiInfosRequest {
    public $Count;
    public $LicenseNum;
    public $LicenseDate;
    public $Name;
    public $OgrnNum;
    public $OgrnDate;
    public $Brand;
    public $Model;
    public $RegNum = 'ЕМ33377';
    public $Year;
    public $BlankNum;
}